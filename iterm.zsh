newtab() {
    osascript \
    -e 'tell application "iTerm2" to tell current window to set newTab to (create tab with default profile)'\
    -e "tell application \"iTerm2\" to tell current session of newTab to write text \"$*\""
}
hx() {
  h "$@ && exit"
}
hf() {
    osascript \
    -e "tell application \"iTerm2\" to tell current window to tell current session to set newSplit to ( split horizontally with default profile )"\
    -e "tell application \"iTerm2\" to tell newSplit to write text \"$*\""\
    -e "tell application \"iTerm2\" to select newSplit"
}
focus() {
    osascript \
    -e "tell application \"iTerm2\" to select session -1"
}
h() {
    osascript \
    -e "tell application \"iTerm2\" to tell current window to tell current session to set newSplit to ( split horizontally with default profile )"\
    -e "tell application \"iTerm2\" to tell newSplit to write text \"$*\""
}
vf() {
    osascript \
    -e "tell application \"iTerm2\" to tell current window to tell current session to set newSplit to ( split vertically with default profile )"\
    -e "tell application \"iTerm2\" to tell newSplit to write text \"$*\""\
    -e "tell application \"iTerm2\" to select newSplit"
}
v() {
    osascript \
    -e "tell application \"iTerm2\" to tell current window to tell current session to set newSplit to ( split vertically with default profile )"\
    -e "tell application \"iTerm2\" to tell newSplit to write text \"$*\""
}
write_line() {
    osascript \
    -e "tell application \"iTerm2\" to tell current window to tell current session to write text \"$*\""
}
write_noline() {
  osascript \
  -e "tell application \"iTerm2\" to tell current window to tell current session to write text \"$*\" newline NO"
}
close_all() {
  osascript \
  -e "set i to 25"\
  -e "repeat while i > 0"\
  -e "tell application \"iTerm2\" to tell current window to tell current session to close"\
  -e "set i to i - 1"\
  -e "end repeat"\
}