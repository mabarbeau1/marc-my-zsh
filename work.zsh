@ () {
  cd $CODE_HOME
  if [ $# -ne 0 ]; then;
    cd $1
  else
    ls
    write_noline 'cd '
  fi
}

dotfiles() {
  cd ~/.dotfiles
  if [ "$1" = "code" ]; then
    code .
  elif [ $# -ne 0 ]; then;
    $@
  fi
}

nuke() {
  find . -name node_modules -exec rm -rf '{}' +
  $@
}

# Docker up
up() {
  (
    cd "docker" || return
    docker compose up $@
  )
}

# Docker build
build() {
  (
    cd "docker" || return
    docker compose build $@
  )
}

# Docker down
down() {
  (
    cd "docker" || return
    docker compose down $@
  )
}

# Auto stop
bye() {
  killall node
  docker kill $(docker ps -q)
  docker container prune --force
  osascript \
  -e "set i to 25"\
  -e "repeat while i > 0"\
  -e "tell application \"iTerm2\" to tell current window to tell current session to close"\
  -e "set i to i - 1"\
  -e "end repeat"\
}
