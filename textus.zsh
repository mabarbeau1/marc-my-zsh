branch() {
  if [ $# -ne 2 ]; then;
    echo "Error"
  elif [ $# -ne 0 ]; then;
    git checkout -b "$1/sc-$2"
  fi
}


feature() {
  branch feature $1
}

bug() {
  branch bug $1
}

chore() {
  branch chore $1
}

textus() {
  cd $CODE_HOME/Work/tesseract

  nvm use v16.20.2
}

textus_elasticsearch_image="tompreston377/elasticsearch"
textus_elasticsearch_name="textus-elasticsearch"
alias textus_elasticsearch="docker ps --all --filter name=${textus_elasticsearch_name} -q"

restart_elastic() {
  process=$(textus_elasticsearch)

  if [ -n "$process" ]; then
    docker kill $(textus_elasticsearch)

    docker container prune --force
  fi

  docker run --name $textus_elasticsearch_name -p 9200:9200 -p 9300:9300 $textus_elasticsearch_image &
}

fresh() {
  (
    textus
    wip
    rebase origin/master $@ && (
      restart_elastic
      
      ./bin/setup
      ./bin/start
    )
  )
}

update() {
  docker -v & (
    textus

    process=$(textus_elasticsearch)

    if [ -n "$process" ]; then
      restart_elastic
    fi

    wip

    rebase origin/master $@ && (
      ./bin/update
      ./bin/start
    )
  )
}

start() {
  docker -v & (
    textus

    ./bin/start
  )
}
