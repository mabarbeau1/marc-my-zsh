# Type less git commands
alias g="git"

# Status
st() {
  git status .
}

# Abort merge or rebase
abort() {
  git merge --abort || git rebase --abort || git cherry-pick --abort
}

# Current Branch
bc() {
  git branch | grep \* | cut -d ' ' -f2
}

## Checkout
co() {
  if [ $# -eq 0 ]; then
    write_noline 'git checkout '
  else
    git checkout $@;
  fi
}

# Push new branch
new() {
  currect_branch=$(git branch | grep \* | cut -d ' ' -f2)
  git push -u origin $currect_branch
}

# You only live once
yolo() {
  message=$(curl -s https://whatthecommit.com/index.txt)
  echo "\n$message"
  if [ "$1" = "push" ]; then
    git add .
    git commit -m "$message";
  else
    select yn in yes no
    do
      case $yn in
        yes)
          git commit -m "$message"; break;;
        no)
          message=$(curl -s https://whatthecommit.com/index.txt)
          echo "\n$message"
      esac
    done  
  fi
  if [ "$1" = "push" ]; then
    git push
  fi
}

wip() {
  git add .
  git commit -m 'WIP' --no-verify
}

undo() {
  git undo
}

# Clean up merged git branches
function gitclean {
  if [[ -z "$1" ]]; then
    echo "Please provide a root branch to use as the reference point for cleaning merged branches"
    return
  fi

  local -r trunk="$1"
  local -r protected_branches="(master$)|(develop$)|(staging$)|(main$)"

  git checkout $trunk

  printf "\nMerged branches to be deleted:\n"
  git branch --merged | grep -Ev $protected_branches
  printf "\nAre you sure you want to delete these branches? (y/N)\n"
  read answer

  if [[ $answer = "y" ]]; then;
    git branch --merged | grep -Ev $protected_branches | xargs git branch -d
  fi
}

rebase () {
  if [ $# -ne 0 ]; then;
    locks=$(find . -name "yarn.lock" -not -path "./node_modules/*" -not -path "*/node_modules/*" | tr '\n' ' ')

    output=$(git rebase $@) && echo $output

    if echo $output | grep 'CONFLICT' | grep -q 'yarn.lock'; then
      echo -e "\n\033[4;33mConflicts found in yarn.lock\n\033[0m\033[2;20mAuto merging\033[0m\n" \
      && git checkout $1 -- $(echo $locks) \
      && yarn install \
      && git add $(echo $locks) \
      echo -e -n "\n\033[6;32mConflicts in yarn.lock resolved\!\n\033[0m\033[2;20mContinue with \`\033[0m" \
      && echo "git rebase --continue\`\n"
    fi
  else
    git rebase $@
  fi
}