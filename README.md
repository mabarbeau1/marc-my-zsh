# Marc My Zsh
Marc's Oh My Zsh customizations

## What is this?

This is a collection of useful bash scripts/ aliases that I have gathered over the years.


## Setup

First setup, Oh My Zsh if you haven't already.

Then, after cloning this repo, define the path to this project as $ZSH_CUSTOM, in `~/.zshrc`

I.E
```bash
ZSH_CUSTOM=/Volumes/Code/Personal/marc-my-zsh
```

## For more details
See: https://github.com/ohmyzsh/ohmyzsh/wiki/Customization
