alias so="source ~/.zshrc"
alias today="date +'%Y-%m-%d'"

chrome () {
  /usr/bin/open -a "/Applications/Google Chrome.app" "$1"
}

confirm_command() {
  success "\n$1"
  select yn in "Yes" "No"; do
      case $yn in
          Yes ) eval $2; break;;
          No ) eval $3; break;;
      esac
  done
}

warning() {
  style='\033[4;33m'
	echo -e -n "${style}$@\033[0m"
}

error() {
  style='\033[6;31m'
	echo -e -n "${style}$@\033[0m"
}

error_h() {
  style='\033[7;31m'
	echo -e -n "${style}$@\033[0m"
}

success() {
  style='\033[6;32m'
	echo -e -n "${style}$@\033[0m"
}

success_h() {
  style='\033[7;32m'
	echo -e -n "${style}$@\033[0m"
}

emphasized() {
  style='\033[3;20m'
	echo -e -n "${style}$@\033[0m"
}

info() {
  style='\033[2;20m'
	echo -e -n "${style}$@\033[0m"
}

strike() {
  style='\033[9;20m'
	echo -e -n "${style}$@\033[0m"
}

hg() {
  history | grep $@
}

dontgiveup() {
  eval $@ || dontgiveup $@
}

marc() {
  cd '/Volumes/Code/Personal/'
  ls 
  write_noline 'cd '
}


forever() {
  eval $@
  forever $@
}


ask_for_command() {
  emphasized "\nPlease type the correct command: "
  read corrected;

  error_h "-$1"
  if [ $# -ne 1 ]; then
    error " ${@:2}"
  fi
  echo

  success_h "+${corrected}"
  if [ $# -ne 1 ]; then
    success " ${@:2} "
  fi
  echo

  emphasized "\nDoes "
  info "\`${corrected}"
  if [ $# -ne 1 ]; then
    info " ${@:2}"
  fi
  emphasized "\` look correct?\n"
}

do_correction() {
  ask_for_command $@


  echo -n "\nSelect: "
  select yn in yes no
    do
      case $yn in
        yes)
          echo "yes"

          info "Aliasing \`${1}'='${corrected}\`\n"

          echo "alias '${1}'='${corrected}'" >> $CODE_HOME/Personal/marc-my-zsh/typo.zsh
          write_line "source $CODE_HOME/Personal/marc-my-zsh/typo.zsh"

          success "\nSuccess! \n"

          write_noline "${corrected} ${@:2}"

          break;;
        no)
          do_correction $@

          break;;

      esac
    done
}


command_not_found_handler() {
    error "\nCommand not found: $@\n";
    info "[Ctrl+C to cancel]\n"

    do_correction $@

    return 127
}